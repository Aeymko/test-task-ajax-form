$(document).ready(function () {
  $("form").submit(function (event) {
    $(".form-group").removeClass("has-error");
    $(".help-block").remove();

    var formData = {
      name: $("#name").val(),
      surname: $("#surname").val(),
      email: $("#email").val(),
      password: $("#password").val(),
      repassword: $("#repassword").val(),
    };

    $.ajax({
      type: "POST",
      url: "mail_service.php",
      data: formData,
      dataType: "json",
      encode: true,
    }).done(function (data) {
      console.log(data);
	  
    if (!data.success) {
        if (data.errors.name) {
          $("#name-group").addClass("has-error");
          $("#name-group").append(
            '<div class="help-block">' + data.errors.name + "</div>"
          );
        }

        if (data.errors.surname) {
          $("#surname-group").addClass("has-error");
          $("#surname-group").append(
            '<div class="help-block">' + data.errors.surname + "</div>"
          );
        }

        if (data.errors.email) {
          $("#email-group").addClass("has-error");
          $("#email-group").append(
            '<div class="help-block">' + data.errors.email + "</div>"
          );
        }

        if (data.errors.password) {
          $("#password-group").addClass("has-error");
          $("#password-group").append(
            '<div class="help-block">' + data.errors.password + "</div>"
          );
        }

        if (data.errors.repassword) {
          $("#repassword-group").addClass("has-error");
          $("#repassword-group").append(
            '<div class="help-block">' + data.errors.repassword + "</div>"
          );
        }
      } else {
        $("form").html(
          '<div class="alert alert-success">' + data.message + "</div>"
        );
      }

    })
    .fail(function (data) {
      $("form").html(
        '<div class="alert alert-danger">Could not reach server, please try again later.</div>'
      );
    });

    event.preventDefault();
  });
});
