<?php

$errors = [];
$data = [];
$users = array (
    "id"  => array(1, 2, 3),
    "name" => array("Олексій", "Дмитро", "Іван"),
    "email" => array("aeymko@gmail.com", "second", "third")
);

if (empty($_POST['name'])) {
    $errors['name'] = "Ім'я не заповнено.";
}

if (empty($_POST['surname'])) {
    $errors['surname'] = "Прізвище не заповнено.";
}

if (empty($_POST['email'])) {
    $errors['email'] = 'Email не заповнено.';
} else {

if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    if (in_array(strtolower($_POST['email']), $users['email'], false)) {
            $errors['email'] = 'Цей Email вже зареєстровано.';
        };
    } else {
        $errors['email'] = "Email адреса не валідна.\n";
    };
}

if (empty($_POST['password'])) {
    $errors['password'] = 'Пароль не заповнено.';
}

if (empty($_POST['repassword'])) {
    $errors['repassword'] = 'Будь ласка, повторіть пароль.';
}

if ($_POST['password'] != $_POST['repassword']) {
    $errors['repassword'] = 'Паролі не співпадають.';
}

$csv_out = $errors;
array_push($csv_out, date('l jS \of F Y h:i:s A'), $_POST['name'], $_POST['surname'], $_POST['email']);

$fp = fopen('file.csv', 'a+');

    fputcsv($fp, $csv_out);

fclose($fp);

if (!empty($errors)) {
    $data['success'] = false;
    $data['errors'] = $errors;
} else {
    $data['success'] = true;
    $data['message'] = 'Форму відправлено успішно!';
}


echo json_encode($data);
